from django.conf.urls import include, url
from django.views.generic.base import TemplateView

from . import views


urlpatterns = [
    url(r'^mappe/(?P<pk>[0-9]+)/ny$', views.MCTMGADCreateView.as_view(), name='mctmgad_create'),
    url(r'^detalj/(?P<pk>[0-9]+)$', views.MCTMGADDetailView.as_view(), name='mctmgad_detail'),
    url(r'^endre/(?P<pk>[0-9]+)$', views.MCTMGADUpdateView.as_view(), name='mctmgad_update')
]