from django.apps import AppConfig


class AssessmentMctmgadConfig(AppConfig):
    name = 'assessment_mctmgad'
    verbose_name = 'Skjema MCTMGAD'
