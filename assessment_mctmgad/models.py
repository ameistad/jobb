from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse

from assessments.models import Folder

class MCTMGADModel(models.Model):
    date = models.DateTimeField(default=timezone.now)
    folder = models.ForeignKey(Folder, related_name='mctmgads')

    trigger = models.TextField(blank=True)
    type1_worry = models.TextField(blank=True)
    type2_worry = models.TextField(blank=True)
    positive_meta_beliefs = models.TextField(blank=True)
    negative_meta_beliefs = models.TextField(blank=True)
    behavior = models.TextField(blank=True)
    thought_control = models.TextField(blank=True)
    emotion = models.TextField(blank=True)


    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'MCT GAD kasusformulering'

    def get_absolute_url(self):
        return reverse('assessments:mctmgad_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return '{}/{}/MCTMGAD-{}'.format(self.folder.user, self.folder.name, self.date)
