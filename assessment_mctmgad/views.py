from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils import timezone

from .models import MCTMGADModel
from assessments.models import Folder


class MCTMGADCreateView(LoginRequiredMixin, generic.CreateView):
    model = MCTMGADModel
    template_name = 'assessment_mctmgad/mctmgad_create.html'
    fields = ['folder', 'trigger', 'type1_worry', 'type2_worry',
              'positive_meta_beliefs', 'negative_meta_beliefs', 
              'behavior', 'thought_control', 'emotion']

    def get(self, request, *args, **kwargs):
        folder = get_object_or_404(Folder, pk=self.kwargs['pk'])

        if folder.user != request.user:
            raise PermissionDenied
        else:
            return super(MCTMGADCreateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MCTMGADCreateView, self).get_context_data(**kwargs)
        context['from_folder'] = Folder.objects.get(pk=self.kwargs['pk'])
        context['other_folders'] = Folder.objects.filter(user=self.request.user).exclude(pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        form.instance.date = timezone.now()
        return super(MCTMGADCreateView, self).form_valid(form)


class MCTMGADDetailView(LoginRequiredMixin, generic.DetailView):
    model = MCTMGADModel
    template_name = 'assessment_mctmgad/mctmgad_detail.html'
    context_object_name = 'mctmgad'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Check if user is owner of content
        if self.object.folder.user != request.user:
            raise PermissionDenied
        else:
            return super(MCTMGADDetailView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()

        return redirect('assessments:folder_detail', self.object.folder.pk)


class MCTMGADUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = MCTMGADModel
    template_name = 'assessment_mctmgad/mctmgad_update.html'
    context_object_name = 'mctmgad'
    fields = ['folder', 'trigger', 'type1_worry', 'type2_worry',
              'positive_meta_beliefs', 'negative_meta_beliefs', 
              'behavior', 'thought_control', 'emotion']

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Check if user is owner of content
        if self.object.folder.user != request.user:
            raise PermissionDenied
        else:
            return super(MCTMGADUpdateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MCTMGADUpdateView, self).get_context_data(**kwargs)
        context['folders'] = Folder.objects.filter(user=self.request.user)
        return context
