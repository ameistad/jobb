from django.db import models
from django.conf import settings
from django.utils import timezone
from django.core.urlresolvers import reverse


class Folder(models.Model):
    name = models.CharField(max_length=200)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['-created_at']
        verbose_name_plural = 'Mapper'

    def __str__(self):
        return '{}/{}'.format(self.user, self.name)

    def get_absolute_url(self):
        return reverse('assessments:folder_detail', kwargs={'pk': self.pk})