from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy
from django.utils import timezone

from .utils import check_user
from .models import Folder
from assessment_bdi.models import BDIModel
from assessment_bai.models import BAIModel
from assessment_pswq.models import PSWQModel
from assessment_abcde.models import ABCDEModel
from assessment_prs.models import PRSModel
from assessment_mctmgad.models import MCTMGADModel
from assessment_mctmd.models import MCTMDModel
from assessment_bcf.models import BCFModel

class CheckUserOwnerMixin(object):
    ''' 
    Check if user is owner of the object
    '''
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.user != request.user:
            raise Http404
        else:
            return super(CheckUserOwnerMixin, self).get(request, *args, **kwargs)

class IndexView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'assessments/index.html'
    
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        folders = Folder.objects.filter(user=self.request.user)
        folders_and_count = []
        for folder in folders:
            folders_and_count.append({
                'pk': folder.pk,
                'name': folder.name, 
                'created_at': folder.created_at,
                'assessments': folder.bdis.count() + 
                               folder.bais.count() +
                               folder.pswqs.count(),
                'schemas': folder.abcdes.count() +
                           folder.prss.count() +
                           folder.mctmds.count() +
                           folder.mctmgads.count() +
                           folder.bcfs.count()
                           
                })
        context['folders_and_count'] = folders_and_count
        return context


class FolderCreateView(LoginRequiredMixin, generic.CreateView):
    model = Folder
    success_url = reverse_lazy('assessments:index')
    fields = ['name']

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.created_at = timezone.now()
        form.save()
        return super(FolderCreateView, self).form_valid(form)

    def get(self):
        raise Http404

class FolderDetailView(LoginRequiredMixin, generic.DetailView):
    model = Folder
    template_name = 'assessments/folder_detail.html'
    context_object_name = 'folder'

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        request = check_user(request, self.object.user)
        return super(FolderDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FolderDetailView, self).get_context_data(**kwargs)
        folder = self.object
        context['bdis_in_folder'] = BDIModel.objects.filter(folder=folder)
        context['bais_in_folder'] = BAIModel.objects.filter(folder=folder)
        context['pswqs_in_folder'] = PSWQModel.objects.filter(folder=folder)
        context['abcdes_in_folder'] = ABCDEModel.objects.filter(folder=folder)
        context['prss_in_folder'] = PRSModel.objects.filter(folder=folder)
        context['mctmgads_in_folder'] = MCTMGADModel.objects.filter(folder=folder)
        context['mctmds_in_folder'] = MCTMDModel.objects.filter(folder=folder)
        context['bcfs_in_folder'] = BCFModel.objects.filter(folder=folder)
        return context

class FolderUpdateView(LoginRequiredMixin, 
                      CheckUserOwnerMixin, 
                      generic.UpdateView):
    model = Folder
    fields = ['name']

    def get(self):
        raise Http404

class FolderDeleteView(LoginRequiredMixin,
                       CheckUserOwnerMixin,
                       generic.DeleteView):
    model = Folder
    success_url = reverse_lazy('assessments:index')

    def get(self):
        raise Http404
