from django.dispatch import receiver
from django.utils import timezone

from registration.signals import user_activated
from .models import Folder


@receiver(user_activated)
def make_drafts_folder(user, **kwargs):
    draft_folder = Folder(
        name='Kladdmappe', 
        user=user, 
        created_at=timezone.now())
    draft_folder.save()
