from django.core.exceptions import PermissionDenied


def check_user(request, object_user):
    # Checks if logged in user is owner of object
    if request.user == object_user:
        return request
    
    raise PermissionDenied
