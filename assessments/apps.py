from django.apps import AppConfig


class AssessmentsConfig(AppConfig):
    name = 'assessments'
    verbose_name ='Kartlegging'

    def ready(self):
        import assessments.signals