from django.conf.urls import include, url
from django.views.generic.base import TemplateView

from . import views
import assessment_abcde.urls
import assessment_bdi.urls
import assessment_bai.urls
import assessment_pswq.urls
import assessment_prs.urls
import assessment_mctmgad.urls
import assessment_mctmd.urls
import assessment_bcf.urls


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^mappe/(?P<pk>[0-9]+)/$', views.FolderDetailView.as_view(), name='folder_detail'),
    url(r'^mappe/endre/(?P<pk>[0-9]+)/$', views.FolderUpdateView.as_view(), name='folder_update'),
    url(r'^mappe/slette/(?P<pk>[0-9]+)/$', views.FolderDeleteView.as_view(), name='folder_delete'),
    url(r'^mappe/ny$', views.FolderCreateView.as_view(), name='folder_create'),
    url(r'^abcde/', include(assessment_abcde.urls)),
    url(r'^bdi/', include(assessment_bdi.urls)),
    url(r'^bai/', include(assessment_bai.urls)),
    url(r'^pswq/', include(assessment_pswq.urls)),
    url(r'^prs/', include(assessment_prs.urls)),
    url(r'^mctmgad/', include(assessment_mctmgad.urls)),
    url(r'^mctmd/', include(assessment_mctmd.urls)),
    url(r'^bcf/', include(assessment_bcf.urls))
]