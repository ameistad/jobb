from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView

import user.urls
import assessments.urls
import sfiles.urls
from aboutpage.views import AboutPageView
from whitewash.views import upload_file


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TemplateView.as_view(template_name='start.html'), name='index'),
    url(r'^omsiden/$', AboutPageView.as_view(), name='aboutpage'),
    url(r'^vask/$', upload_file, name='whitewash'),
    url(r'^kartlegging/', include(assessments.urls, namespace='assessments')),
    url(r'^papir/', include(sfiles.urls, namespace='sfiles')),
    url(r'^bruker/', include(user.urls, namespace='user'))
]
