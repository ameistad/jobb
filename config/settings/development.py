# -*- coding: utf-8 -*-
# Development settings

from .base import *  # noqa


DEBUG = True

# Django debug toolbar
#INSTALLED_APPS += ('debug_toolbar', )

ALLOWED_HOSTS = []

# Email
EMAIL_PORT = 1025

EMAIL_HOST = 'localhost'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# DEFAULT_FROM_EMAIL = 'hei@amei.io'
# EMAIL_BACKEND = 'django_mailgun.MailgunBackend'
# MAILGUN_ACCESS_KEY = env('MAILGUN_ACCESS_KEY')
# MAILGUN_SERVER_NAME = env('MAILGUN_SERVER_NAME')

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# DEFAULT_FROM_EMAIL = 'testing@example.com'
# EMAIL_HOST_USER = ''
# EMAIL_HOST_PASSWORD = ''
# EMAIL_USE_TLS = False
# EMAIL_PORT = 1025


# Celery
#CELERY_ALWAYS_EAGER = True
BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Oslo'
