# -*- coding: utf-8 -*-
"""
Django settings for jobb.

For more information on this file, see
https://docs.djangoproject.com/en/stable/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/stable/ref/settings/
"""

import os
import environ


# Django-environ for using 12-factor environment variables.
# http://12factor.net/)
env = environ.Env()

# .env file to store environment variables.
env_file = os.path.join(os.path.dirname(__file__), 'development.env')

if os.path.exists(env_file):
    environ.Env.read_env(str(env_file))

# Build paths inside the project like this: join(BASE_DIR, "directory")
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# Secret key from environment variables
# https://docs.djangoproject.com/en/stable/ref/settings/#secret-key
SECRET_KEY = env('DJANGO_SECRET_KEY')

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    
    # Third party apps
    'storages',

    # Own apps
    'user',
    'aboutpage',
    'whitewash',
    'sfiles',
    'assessments',
    'assessment_abcde',
    'assessment_bdi',
    'assessment_bai',
    'assessment_pswq',
    'assessment_prs',
    'assessment_mctmgad',
    'assessment_mctmd',
    'assessment_bcf'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
            ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.static',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

# Database
# https://docs.djangoproject.com/en/stable/ref/settings/#databases
# Get databases from DATABASE_URL.
# https://django-environ.readthedocs.org/en/latest/
DATABASES = {
    'default': env.db(),
}

# Internationalization
# https://docs.djangoproject.com/en/stable/topics/i18n/
LANGUAGE_CODE = 'nb'

TIME_ZONE = 'Europe/Oslo'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Managers
# https://docs.djangoproject.com/en/stable/ref/settings/#managers
ADMINS = (
    ("""Andreas Meistad""", 'ameistad@gmail.com'),
)

MANAGERS = ADMINS


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/stable/howto/static-files/
STATIC_URL = '/static/'

STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

STATIC_ROOT = 'staticfiles'

# Media
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'


# Registration
ACCOUNT_ACTIVATION_DAYS = 7
LOGIN_URL = 'user:login'
LOGIN_REDIRECT_URL = '/'
