from django.apps import AppConfig


class AssessmentBaiConfig(AppConfig):
    name = 'assessment_bai'
    verbose_name = 'Kartlegging BAI'
