from django import template

register = template.Library()

@register.filter(name='bai_verbal')
def bai_verbal(score):
    if score <= 7:
        return 'minimalt angstnivå'
    elif score <= 15:
        return 'noe hevet angstnivå'
    elif score <= 25:
        return 'moderat hevet angstnivå'
    else:
        return 'alvorlig hevet angstnivå'
