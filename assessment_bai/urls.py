from django.conf.urls import include, url

from . import views


urlpatterns = [
    url(r'^mappe/(?P<pk>[0-9]+)/ny$', views.BAICreateView.as_view(), name='bai_create'),
    url(r'^detalj/(?P<pk>[0-9]+)$', views.BAIDetailView.as_view(), name='bai_detail'),
    url(r'^endre/(?P<pk>[0-9]+)$', views.BAIUpdateView.as_view(), name='bai_update'),
    url(r'^mappe/(?P<pk>[0-9]+)/bailagret$', views.BAIFinishedView.as_view(), name='bai_finished')
]