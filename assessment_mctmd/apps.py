from django.apps import AppConfig


class AssessmentMctmdConfig(AppConfig):
    name = 'assessment_mctmd'
    verbose_name = 'Skjema MCTMD'
