from django.conf.urls import include, url
from django.views.generic.base import TemplateView

from . import views


urlpatterns = [
    url(r'^mappe/(?P<pk>[0-9]+)/ny$', views.MCTMDCreateView.as_view(), name='mctmd_create'),
    url(r'^detalj/(?P<pk>[0-9]+)$', views.MCTMDDetailView.as_view(), name='mctmd_detail'),
    url(r'^endre/(?P<pk>[0-9]+)$', views.MCTMDUpdateView.as_view(), name='mctmd_update')
]