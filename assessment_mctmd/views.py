from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils import timezone

from .models import MCTMDModel
from assessments.models import Folder


class MCTMDCreateView(LoginRequiredMixin, generic.CreateView):
    model = MCTMDModel
    template_name = 'assessment_mctmd/mctmd_create.html'
    fields = ['folder', 'trigger', 'rumination', 'positive_meta_beliefs',
              'negative_meta_beliefs', 'depression_behavior',
              'depression_thoughts', 'depression_affect']

    def get(self, request, *args, **kwargs):
        folder = get_object_or_404(Folder, pk=self.kwargs['pk'])

        if folder.user != request.user:
            raise PermissionDenied
        else:
            return super(MCTMDCreateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MCTMDCreateView, self).get_context_data(**kwargs)
        context['from_folder'] = Folder.objects.get(pk=self.kwargs['pk'])
        context['other_folders'] = Folder.objects.filter(user=self.request.user).exclude(pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        form.instance.date = timezone.now()
        return super(MCTMDCreateView, self).form_valid(form)

class MCTMDDetailView(LoginRequiredMixin, generic.DetailView):
    model = MCTMDModel
    template_name = 'assessment_mctmd/mctmd_detail.html'
    context_object_name = 'mctmd'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Check if user is owner of content
        if self.object.folder.user != request.user:
            raise PermissionDenied
        else:
            return super(MCTMDDetailView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()

        return redirect('assessments:folder_detail', self.object.folder.pk)


class MCTMDUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = MCTMDModel
    template_name = 'assessment_mctmd/mctmd_update.html'
    context_object_name = 'mctmd'
    fields = ['folder', 'trigger', 'rumination', 'positive_meta_beliefs',
              'negative_meta_beliefs', 'depression_behavior',
              'depression_thoughts', 'depression_affect']

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Check if user is owner of content
        if self.object.folder.user != request.user:
            raise PermissionDenied
        else:
            return super(MCTMDUpdateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MCTMDUpdateView, self).get_context_data(**kwargs)
        context['folders'] = Folder.objects.filter(user=self.request.user)
        return context
