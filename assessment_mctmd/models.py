from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse

from assessments.models import Folder

class MCTMDModel(models.Model):
    date = models.DateTimeField(default=timezone.now)
    folder = models.ForeignKey(Folder, related_name='mctmds')

    trigger = models.TextField(blank=True)
    rumination = models.TextField(blank=True)
    positive_meta_beliefs = models.TextField(blank=True)
    negative_meta_beliefs = models.TextField(blank=True)
    depression_behavior = models.TextField(blank=True)
    depression_thoughts = models.TextField(blank=True)
    depression_affect = models.TextField(blank=True)


    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'MCT Depresjon kasusformulering'

    def get_absolute_url(self):
        return reverse('assessments:mctmd_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return '{}/{}/MCTMD-{}'.format(self.folder.user, self.folder.name, self.date)
