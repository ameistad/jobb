$("#fallacies").find("a").click(
    function () {
        $("a").removeClass("btn-custom-small-click")
        $("#fallacies-info>*").hide().filter($(this).data("filter")).show()
        $(this).addClass("btn-custom-small-click")
});

$('#fallacies-info').find("a").click(
    function() {    
        var value = $(this).data('fallacy');
        var input = $("#id_logical_fallacies");
        input.val(input.val() + value + ', ');
        return false;
});
