from django.conf.urls import include, url
from django.views.generic.base import TemplateView

from . import views


urlpatterns = [
    url(r'^mappe/(?P<pk>[0-9]+)/ny$', views.prs_create_view, name='prs_create'),
    url(r'^detalj/(?P<pk>[0-9]+)$', views.PRSDetailView.as_view(), name='prs_detail'),
    url(r'^endre/(?P<pk>[0-9]+)$', views.prs_update_view, name='prs_update'),
    url(r'^mappe/(?P<pk>[0-9]+)/oppsummering$', views.PRSSummaryView.as_view(), 
        name='prs_summary'),
]