from django import template

register = template.Library()

@register.filter(name='check_even')
def check_even(number):
    return number % 2 == 0

# returns question text
static_question_list = {
        'question0301': 'Setter meg ned',
        'question0302': 'Distraherer meg',
        'question0303': 'Kontrollerer pusten',
        'question0304': 'Tar pulsen',
        'question0305': 'Prøver å slappe av',
        'question0306': 'Leter etter utgangen',
        'question0307': 'Kontrollerer sinnet mitt',
        'question0308': 'Beveger meg roligere',
        'question0309': 'Tar beroligende medisiner',   
        'question0310': 'Holder fast i noe eller noen',
        'question0311': 'Forlater situasjonen',
        'question0312': 'Prøver å være sammen med noen',
        'question0401': 'Jeg får hjerteinfarkt',
        'question0402': 'Jeg er i ferd med å bli gal',
        'question0403': 'Jeg dør',
        'question0404': 'Jeg får slag',
        'question0405': 'Jeg får ikke luft',
        'question0406': 'Jeg kveles',
        'question0407': 'Jeg kollapser',
        'question0408': 'Jeg blir blind',
        'question0409': 'Jeg besvimer',
        'question0410': 'Jeg blir lammet',
        'question0411': 'Jeg mister kontrollen',
        'question0412': 'Jeg kommer til å skrike',
        'question0413': 'Jeg kommer til å kaste opp',
        'question0414': 'Panikkanfallet tar aldri slutt'
}
@register.filter(name='static_question')
def static_question(question):
    try:
        return static_question_list[question]
    except KeyError:
        return 'Hau'
