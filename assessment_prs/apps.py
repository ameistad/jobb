from django.apps import AppConfig


class AssessmentPrsConfig(AppConfig):
    name = 'assessment_prs'
    verbose_name = 'Skjema PRS'
