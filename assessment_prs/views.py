from collections import defaultdict

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from django.forms import modelform_factory, formset_factory
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import transaction, IntegrityError

from .models import PRSModel, PRSQ3CustomModel, PRSQ4CustomModel
from .forms import ExtraQuestionForm
from assessments.models import Folder

from .utils import q3s_standard_temp, q4s_standard_temp, get_standard_qs


@login_required
def prs_create_view(request, pk):

    # Check if user is owner of folder
    folder = get_object_or_404(Folder, pk=pk)
    
    if folder.user != request.user:
        raise PermissionDenied

    # Get inital data from latest PRS and set extra formsets
    try:
        latest_prs = PRSModel.objects.filter(folder=pk).latest('date')
        
        latest_q3 = PRSQ3CustomModel.objects.filter(standard_model=latest_prs)
        initial_q3 = [{'question03_extra_text': q.question03_extra_text,
                       'question03_extra_score': q.question03_extra_score} 
                       for q in latest_q3]
        
        latest_q4 = PRSQ4CustomModel.objects.filter(standard_model=latest_prs)
        initial_q4 = [{'question04_extra_text': q.question04_extra_text,
                       'question04_extra_score': q.question04_extra_score} 
                       for q in latest_q4]

        extra_q3 = 0 if initial_q3 else 1
        extra_q4 = 0 if initial_q4 else 1

    except ObjectDoesNotExist:
        initial_q3 = initial_q4 = None
        extra_q3 = extra_q4 = 1


    # Generate form from model
    PRSForm = modelform_factory(PRSModel, exclude=('date',))

    # Generate formsets
    PRSQ3FormSet = formset_factory(ExtraQuestionForm, extra=extra_q3)
    PRSQ4FormSet = formset_factory(ExtraQuestionForm, extra=extra_q4)

    # Validate and save forms on post
    if request.method == 'POST':
        prs_form = PRSForm(request.POST)

        if prs_form.is_valid():
            new_prs_form = prs_form.save(commit=False)
            new_prs_form.date = timezone.now()
            new_prs_form.save()

            prs_q3_formset = PRSQ3FormSet(request.POST, prefix='q3_prefix')
            prs_q4_formset = PRSQ4FormSet(request.POST, prefix='q4_prefix')

            if prs_q3_formset.is_valid() and prs_q4_formset.is_valid():
                # Get data from formsets
                q3_data = []
                for field in prs_q3_formset:
                    text = field.cleaned_data.get('text')
                    score = field.cleaned_data.get('score')

                    if text:
                        q3_data.append(PRSQ3CustomModel(
                            standard_model=new_prs_form,
                            question03_extra_text=text,
                            question03_extra_score=score))
                
                q4_data = []
                for field in prs_q4_formset:
                    text = field.cleaned_data.get('text')
                    score = field.cleaned_data.get('score')

                    if text:
                        q4_data.append(PRSQ4CustomModel(
                            standard_model=new_prs_form,
                            question04_extra_text=text,
                            question04_extra_score=score))
                        
                # Bulk create formsets
                try:
                    with transaction.atomic():
                        PRSQ3CustomModel.objects.bulk_create(q3_data)
                        PRSQ4CustomModel.objects.bulk_create(q4_data)

                except IntegrityError:
                    print('IntegrityError')


        return redirect('assessments:prs_detail', new_prs_form.pk)

    else:
        prs_form = PRSForm()
        prs_q3_formset = PRSQ3FormSet(initial=initial_q3, prefix='q3_prefix')
        prs_q4_formset = PRSQ4FormSet(initial=initial_q4, prefix='q4_prefix')

        other_folders = Folder.objects.filter(user=request.user).exclude(pk=pk)

        context = {
            'prs_form': prs_form,
            'prs_q3_formset': prs_q3_formset,
            'prs_q4_formset': prs_q4_formset,
            'from_folder': folder,
            'other_folders': other_folders
        }

        return render(request, 'assessment_prs/prs_create.html', context)


@login_required
def prs_update_view(request, pk):

    prs = get_object_or_404(PRSModel, pk=pk)

    # Check if user is owner of prs folder
    if prs.folder.user != request.user:
        raise PermissionDenied

    # Get inital data from latest PRS.
    try:
        q3_in_prs = PRSQ3CustomModel.objects.filter(standard_model=prs)
        initial_q3 = [{'question03_extra_text': q.question03_extra_text,
                       'question03_extra_score': q.question03_extra_score} 
                       for q in q3_in_prs]
        
        q4_in_prs = PRSQ4CustomModel.objects.filter(standard_model=prs)
        initial_q4 = [{'question04_extra_text': q.question04_extra_text,
                       'question04_extra_score': q.question04_extra_score} 
                       for q in q4_in_prs]

        extra_q3 = 0 if initial_q3 else 1
        extra_q4 = 0 if initial_q4 else 1

    except ObjectDoesNotExist:
        print('ObjectDoesNotExist!')
        initial_q3 = None
        initial_q4 = None
        extra_q3 = 1
        extra_q4 = 1


    # Generate forms from models
    PRSForm = modelform_factory(PRSModel, exclude=('date',))
    PRSQ3FormSet = formset_factory(ExtraQuestionForm, extra=extra_q3)
    PRSQ4FormSet = formset_factory(ExtraQuestionForm, extra=extra_q4)

    # Validate and save forms
    if request.method == 'POST':
        prs_form = PRSForm(request.POST)

        if prs_form.is_valid(): 
            new_prs_form = prs_form.save(commit=False)
            new_prs_form.date = prs.date
            new_prs_form.save()

            prs_q3_formset = PRSQ3FormSet(request.POST, prefix='q3_prefix')
            prs_q4_formset = PRSQ4FormSet(request.POST, prefix='q4_prefix')

            if prs_q3_formset.is_valid() and prs_q4_formset.is_valid():
                # Get data from formsets
                q3_data = []
                for field in prs_q3_formset:
                    text = field.cleaned_data.get('text')
                    score = field.cleaned_data.get('score')

                    if text:
                        q3_data.append(PRSQ3CustomModel(
                            standard_model=new_prs_form,
                            question03_extra_text=text,
                            question03_extra_score=score))
                
                q4_data = []
                for field in prs_q4_formset:
                    text = field.cleaned_data.get('text')
                    score = field.cleaned_data.get('score')

                    if text:
                        q4_data.append(PRSQ4CustomModel(
                            standard_model=new_prs_form,
                            question04_extra_text=text,
                            question04_extra_score=score))

                # Save formsets to models with bulk_create
                try:
                    with transaction.atomic():
                        PRSModel.objects.get(pk=prs.pk).delete()

                        PRSQ3CustomModel.objects.bulk_create(q3_data)
                        PRSQ4CustomModel.objects.bulk_create(q4_data)

                except IntegrityError:
                    print('IntegrityError')

        return redirect('assessments:prs_detail', new_prs_form.pk)

    else:
        prs_form = PRSForm()
        prs_q3_formset = PRSQ3FormSet(initial=initial_q3, prefix='q3_prefix')
        prs_q4_formset = PRSQ4FormSet(initial=initial_q4, prefix='q4_prefix')

        other_folders = Folder.objects.filter(
            user=request.user).exclude(pk=prs.folder.pk)

        context = {
            'prs': prs,
            'prs_form': prs_form,
            'prs_q3_formset': prs_q3_formset,
            'prs_q4_formset': prs_q4_formset,
            'other_folders': other_folders
        }

        return render(request, 'assessment_prs/prs_update.html', context)

class PRSDetailView(LoginRequiredMixin, generic.DetailView):
    model = PRSModel
    template_name = 'assessment_prs/prs_detail.html'
    context_object_name = 'prs'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Check if user is owner of content
        if self.object.folder.user != request.user:
            raise PermissionDenied
        else:
            return super(PRSDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PRSDetailView, self).get_context_data(**kwargs)
        context['question03_extra'] = PRSQ3CustomModel.objects.filter(
            standard_model=self.object)
        context['question04_extra'] = PRSQ4CustomModel.objects.filter(
            standard_model=self.object)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()

        return redirect('assessments:folder_detail', self.object.folder.pk)


class PRSSummaryView(LoginRequiredMixin, generic.DetailView):
    model = Folder
    template_name = 'assessment_prs/prs_summary.html'
    context_object_name = 'folder'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.user != request.user:
            raise PermissionDenied
        else:
            return super(PRSSummaryView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PRSSummaryView, self).get_context_data(**kwargs)
        
        all_prs = PRSModel.objects.filter(folder=self.object).order_by('date')
        q3s_in_folder = PRSQ3CustomModel.objects.filter(
            standard_model__folder=self.object).order_by('standard_model__date')
        q4s_in_folder = PRSQ4CustomModel.objects.filter(
            standard_model__folder=self.object).order_by('standard_model__date')
        prs_dates = [prs.date for prs in all_prs]
        

        # Get unique q3s with adjusted zeros.
        q3s_text = defaultdict(list)
        for q3 in q3s_in_folder:
            if q3.question03_extra_text not in q3s_text:
                q3s_text[q3.question03_extra_text] = prs_dates

        # for key, dates in q3s_text.items():
        #     temp_scores = []
        #     for date in dates:
        #         date_found = False
        #         for q3 in q3s_in_folder:
        #             if key == q3.question03_extra_text and date == q3.standard_model.date:
        #                 temp_scores.append(q3.question03_extra_score)
        #                 date_found = True
        #         if not date_found:
        #             temp_scores.append(0)

        # Idiomatic?
        for key, dates in q3s_text.items():
            temp_scores = []
            for date in dates:
                for q3 in q3s_in_folder:
                    if key == q3.question03_extra_text and date == q3.standard_model.date:
                        temp_scores.append(q3.question03_extra_score)
                        break
                else:
                    temp_scores.append(0)

            q3s_text[key] = temp_scores


        q3s_text_scores = []
        for key in q3s_text:
            q3s_text_scores.append({
                'text': key,
                'scores': q3s_text[key]
                })

        # Q4 extras
        q4s_text = defaultdict(list)
        for q4 in q4s_in_folder:
            if q4.question04_extra_text not in q4s_text:
                q4s_text[q4.question04_extra_text] = prs_dates

        # for key, dates in q4s_text.items():
        #     temp_scores = []
        #     for date in dates:
        #         date_found = False
        #         for q4 in q4s_in_folder:
        #             if key == q4.question04_extra_text and date == q4.standard_model.date:
        #                 temp_scores.append(q4.question04_extra_score)
        #                 date_found = True
        #         if not date_found:
        #             temp_scores.append(0)

        for key, dates in q4s_text.items():
            temp_scores = []
            for date in dates:
                for q4 in q4s_in_folder:
                    if key == q4.question04_extra_text and date == q4.standard_model.date:
                        temp_scores.append(q4.question04_extra_score)
                        break
                else:
                    temp_scores.append(0)

            q4s_text[key] = temp_scores


        q4s_text_scores = []
        for key in q4s_text:
            q4s_text_scores.append({
                'text': key,
                'scores': q4s_text[key]
                })


        q3s_standard_questions = get_standard_qs(q3s_standard_temp(all_prs))
        q4s_standard_questions = get_standard_qs(q4s_standard_temp(all_prs))


        context['all_prs_in_folder'] = all_prs
        context['q3s_standard_questions'] = q3s_standard_questions
        context['q4s_standard_questions'] = q4s_standard_questions
        context['q3s_scores'] = q3s_text_scores
        context['q4s_scores'] = q4s_text_scores
        return context
