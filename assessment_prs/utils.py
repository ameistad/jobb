# For PRSSummaryView
def q3s_standard_temp(all_prs):
    q3s_standard_temp = {
        'Setter meg ned': [prs.question0301 for prs in all_prs],
        'Distraherer meg': [prs.question0302 for prs in all_prs],
        'Kontrollerer pusten': [prs.question0303 for prs in all_prs],
        'Tar pulsen': [prs.question0304 for prs in all_prs],
        'Prøver å slappe av': [prs.question0305 for prs in all_prs],
        'Leter etter utgangen': [prs.question0306 for prs in all_prs],
        'Kontrollerer sinnet mitt': [prs.question0307 for prs in all_prs],
        'Beveger meg roligere': [prs.question0308 for prs in all_prs],
        'Tar beroligende medisiner': [prs.question0309 for prs in all_prs],
        'Holder fast i noe eller noen': [prs.question0310 for prs in all_prs],
        'Forlater situasjonen': [prs.question0311 for prs in all_prs],
        'Prøver å være sammen med noen': [prs.question0312 for prs in all_prs]
        }
    return q3s_standard_temp

def q4s_standard_temp(all_prs):
    q4s_standard_temp = {
        'Jeg får hjerteinfarkt': [prs.question0401 for prs in all_prs],
        'Jeg er i ferd med å bli gal': [prs.question0402 for prs in all_prs],
        'Jeg dør': [prs.question0403 for prs in all_prs],
        'Jeg får slag': [prs.question0404 for prs in all_prs],
        'Jeg får ikke luft': [prs.question0405 for prs in all_prs],
        'Jeg kveles': [prs.question0406 for prs in all_prs],
        'Jeg kollapser': [prs.question0407 for prs in all_prs],
        'Jeg blir blind': [prs.question0408 for prs in all_prs],
        'Jeg besvimer': [prs.question0409 for prs in all_prs],
        'Jeg blir lammet': [prs.question0410 for prs in all_prs],
        'Jeg mister kontrollen': [prs.question0411 for prs in all_prs],
        'Jeg kommer til å skrike': [prs.question0412 for prs in all_prs],
        'Jeg kommer til å kaste opp': [prs.question0413 for prs in all_prs],
        'Panikkanfallet tar aldri slutt': [prs.question0414 for prs in all_prs]
        }
    return q4s_standard_temp

# Return a list of items that has values other than zero.
def get_standard_qs(standard_temp):
    standard_qs = []
    for key in standard_temp:
        add_item = False
        for value in standard_temp[key]:
            if value != 0:
                add_item = True
        if add_item:
            standard_qs.append({
                'text': key,
                'scores': standard_temp[key]
            })
    return standard_qs


