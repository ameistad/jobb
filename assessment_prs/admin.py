from django.contrib import admin

from .models import PRSModel, PRSQ3CustomModel, PRSQ4CustomModel


admin.site.register(PRSModel)
admin.site.register(PRSQ3CustomModel)
admin.site.register(PRSQ4CustomModel)