from django import forms

class ExtraQuestionForm(forms.Form):
    text = forms.CharField(required=False)
    score = forms.IntegerField(required=False)
