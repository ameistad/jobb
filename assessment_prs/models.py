from django.db import models
from django.conf import settings
from django.utils import timezone
from django.core.urlresolvers import reverse

from assessments.models import Folder


class PRSModel(models.Model):
    date = models.DateTimeField(default=timezone.now)
    folder = models.ForeignKey(Folder, related_name='prss')

    question01 = models.PositiveSmallIntegerField(default=0)
    question02 = models.PositiveSmallIntegerField(default=0)

    question0301 = models.PositiveSmallIntegerField(default=0)
    question0302 = models.PositiveSmallIntegerField(default=0)
    question0303 = models.PositiveSmallIntegerField(default=0)
    question0304 = models.PositiveSmallIntegerField(default=0)
    question0305 = models.PositiveSmallIntegerField(default=0)
    question0306 = models.PositiveSmallIntegerField(default=0)
    question0307 = models.PositiveSmallIntegerField(default=0)
    question0308 = models.PositiveSmallIntegerField(default=0)
    question0309 = models.PositiveSmallIntegerField(default=0)
    question0310 = models.PositiveSmallIntegerField(default=0)
    question0311 = models.PositiveSmallIntegerField(default=0)
    question0312 = models.PositiveSmallIntegerField(default=0)
    
    question0401 = models.PositiveSmallIntegerField(default=0)
    question0402 = models.PositiveSmallIntegerField(default=0)
    question0403 = models.PositiveSmallIntegerField(default=0)
    question0404 = models.PositiveSmallIntegerField(default=0)
    question0405 = models.PositiveSmallIntegerField(default=0)
    question0406 = models.PositiveSmallIntegerField(default=0)
    question0407 = models.PositiveSmallIntegerField(default=0)
    question0408 = models.PositiveSmallIntegerField(default=0)
    question0409 = models.PositiveSmallIntegerField(default=0)
    question0410 = models.PositiveSmallIntegerField(default=0)
    question0411 = models.PositiveSmallIntegerField(default=0)
    question0412 = models.PositiveSmallIntegerField(default=0)
    question0413 = models.PositiveSmallIntegerField(default=0)
    question0414 = models.PositiveSmallIntegerField(default=0)
    
    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'PRS-skjemaer'
    
    def __str__(self):
        return '{}/{}/PRS-{}'.format(self.folder.user, self.folder.name, self.date)

class PRSQ3CustomModel(models.Model):  
    standard_model = models.ForeignKey(PRSModel)
    question03_extra_text = models.TextField(blank=True)
    question03_extra_score = models.PositiveSmallIntegerField(default=0, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Andre strategier'

    def __str__(self):
        return '{}/{}'.format(self.standard_model, self.question03_extra_text)

class PRSQ4CustomModel(models.Model):
    standard_model = models.ForeignKey(PRSModel)
    question04_extra_text = models.TextField(blank=True)
    question04_extra_score = models.PositiveSmallIntegerField(default=0, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Andre antakelser'

    def __str__(self):
        return '{}/{}'.format(self.standard_model, self.question04_extra_text)
