import os

from django.core.mail import EmailMessage


APPDIR = os.path.dirname(os.path.abspath(__file__))
tempimagefolder = os.path.join(APPDIR, 'tempimagefolder')

def imagemagick_and_send(save_name, user_email):
    in_image = os.path.join(tempimagefolder, save_name)
    out_image = os.path.join(tempimagefolder, 'vasket_' + save_name)

    # Process with ImageMagick
    command = 'convert {} -morphology Convolve DoG:15,100,0 -negate -normalize -blur 0x1 -channel RBG -level 60%,91%,0.1 {}'.format(
        in_image, 
        out_image)
    os.system(command)


    # Send the processed image to user
    email_message = EmailMessage(
        'Vasket bilde',
        'Hei, sender {} som vedlegg.'.format('vasket_' + save_name ),
        'Bildevasker' '<vask@mg.amei.io>',
        [user_email],
        headers = { 'Reply-To': 'vask@mg.amei.io' }
        
    )
    email_message.attach_file(out_image)
    email_message.send()

    # Delete the images after sending
    os.remove(os.path.join(tempimagefolder, save_name))
    os.remove(os.path.join(tempimagefolder, 'vasket_' + save_name))
