from __future__ import absolute_import

from celery.decorators import task
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

from .image import imagemagick_and_send

@task(name="imagemagick_and_send_task")
def imagemagick_and_send_task(save_name, user_email):
    logger.info("Imagemagick and send is queued to celery")
    return imagemagick_and_send(save_name, user_email)


