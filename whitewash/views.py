# -*- coding: utf-8 -*-
import os

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from .forms import ImageUploadForm
from .tasks import imagemagick_and_send_task

APPDIR = os.path.dirname(os.path.abspath(__file__))
tempimagefolder = os.path.join(APPDIR, 'tempimagefolder')

def humansize(nbytes):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    if nbytes == 0: return '0 B'
    i = 0
    while nbytes >= 1024 and i < len(suffixes)-1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])

def save_image(image, save_name):
    with open(os.path.join(tempimagefolder, save_name), 'wb+') as destination:
        for chunk in image.chunks():
            destination.write(chunk)

@login_required
def upload_file(request):
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form_image = request.FILES['imagefile']
            save_name = '{0}_{1}'.format(
                request.user.username,
                form_image.name.replace(' ', '_'))

            # If image file size is larger than 6mb it returns an error.
            if form_image.size > 6291456:
                messages.error(request, 
                    "{0} er for stor ({1}), det er vel ikke nødvendig med så store bilder?".format(
                    form_image.name, humansize(form_image.size)))
                return HttpResponseRedirect(reverse('whitewash'))
            else:
                save_image(form_image, save_name)
                user_email = request.user.email

                # Send as a task to Celery
                imagemagick_and_send_task.delay(save_name, user_email)

                messages.success(request, 
                    '{save_name} ({image_file_size}) sendt til {user_email}'.format(
                    save_name = "vasket_" + save_name,
                    image_file_size = humansize(form_image.size),
                    user_email = user_email))
                return HttpResponseRedirect(reverse('whitewash'))
    else:
        form = ImageUploadForm()

    user_email = request.user.email
    return render(request, 'whitewash/upload.html', {'form': form})
