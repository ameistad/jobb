from django import template

register = template.Library()

@register.filter(name='bdi_verbal')
def bdi_verbal(score):
    if score <= 9:
        return 'ingen depresjon'
    elif score <= 16:
        return 'mild depresjon'
    elif score <= 29:
        return 'moderat depresjon'
    elif score <= 63:
        return 'alvorlig depresjon'
    else:
        return 'Over 63, må være en feil'
