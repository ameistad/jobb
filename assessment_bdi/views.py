from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils import timezone

from .models import BDIModel
from assessments.models import Folder


class BDICreateView(LoginRequiredMixin, generic.CreateView):
    model = BDIModel
    template_name = 'assessment_bdi/bdi_create.html'
    fields = ['folder', 'question01', 'question02', 'question03', 'question04',
            'question05', 'question06', 'question07', 'question08','question09', 
            'question10', 'question11', 'question12', 'question13', 'question14', 
            'question15', 'question16','question17', 'question18', 'question19', 
            'question19a', 'question20', 'question21']

    def get(self, request, *args, **kwargs):
        folder = get_object_or_404(Folder, pk=self.kwargs['pk'])


        if folder.user != request.user:
            raise PermissionDenied
        else:
            return super(BDICreateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BDICreateView, self).get_context_data(**kwargs)
        context['from_folder'] = Folder.objects.get(pk=self.kwargs['pk'])
        context['other_folders'] = Folder.objects.filter(
            user=self.request.user).exclude(pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        form.instance.date = timezone.now()
        form.instance.score = (form.cleaned_data['question01'] 
            + form.cleaned_data['question02'] + form.cleaned_data['question03']
            + form.cleaned_data['question04'] + form.cleaned_data['question05']
            + form.cleaned_data['question06'] + form.cleaned_data['question07']
            + form.cleaned_data['question08'] + form.cleaned_data['question09']
            + form.cleaned_data['question10'] + form.cleaned_data['question11']
            + form.cleaned_data['question12'] + form.cleaned_data['question13']
            + form.cleaned_data['question14'] + form.cleaned_data['question15']
            + form.cleaned_data['question16'] + form.cleaned_data['question17']
            + form.cleaned_data['question18'] + form.cleaned_data['question19']
            + form.cleaned_data['question20'] + form.cleaned_data['question21'])
        form.save()
        return super(BDICreateView, self).form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('assessments:bdi_finished', kwargs={'pk': self.object.folder.pk})

class BDIFinishedView(LoginRequiredMixin, generic.DetailView):
    model = Folder
    template_name = 'assessment_bdi/bdi_finished.html'
    context_object_name = 'folder'

class BDIDetailView(LoginRequiredMixin, generic.DetailView):
    model = BDIModel
    template_name = 'assessment_bdi/bdi_detail.html'
    context_object_name = 'bdi'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Check if user is owner of content
        if self.object.folder.user != request.user:
            raise PermissionDenied
        else:
            return super(BDIDetailView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()

        return redirect('assessments:folder_detail', self.object.folder.pk)


class BDIUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = BDIModel
    template_name = 'assessment_bdi/bdi_update.html'
    context_object_name = 'bdi'
    fields = ['folder', 'question01', 'question02', 'question03', 'question04',
            'question05', 'question06', 'question07', 'question08','question09', 
            'question10', 'question11', 'question12', 'question13', 'question14', 
            'question15', 'question16','question17', 'question18', 'question19', 
            'question19a', 'question20', 'question21']

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Check if user is owner of content
        if self.object.folder.user != request.user:
            raise PermissionDenied
        else:
            return super(BDIUpdateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BDIUpdateView, self).get_context_data(**kwargs)
        context['folders'] = Folder.objects.filter(user=self.request.user)
        return context

    def form_valid(self, form):
        form.instance.score = (form.cleaned_data['question01'] 
            + form.cleaned_data['question02'] + form.cleaned_data['question03']
            + form.cleaned_data['question04'] + form.cleaned_data['question05']
            + form.cleaned_data['question06'] + form.cleaned_data['question07']
            + form.cleaned_data['question08'] + form.cleaned_data['question09']
            + form.cleaned_data['question10'] + form.cleaned_data['question11']
            + form.cleaned_data['question12'] + form.cleaned_data['question13']
            + form.cleaned_data['question14'] + form.cleaned_data['question15']
            + form.cleaned_data['question16'] + form.cleaned_data['question17']
            + form.cleaned_data['question18'] + form.cleaned_data['question19']
            + form.cleaned_data['question20'] + form.cleaned_data['question21'])
        form.save()
        return super(BDIUpdateView, self).form_valid(form)
