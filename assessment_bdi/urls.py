from django.conf.urls import include, url

from . import views


urlpatterns = [
    url(r'^mappe/(?P<pk>[0-9]+)/ny$', views.BDICreateView.as_view(), name='bdi_create'),
    url(r'^detalj/(?P<pk>[0-9]+)$', views.BDIDetailView.as_view(), name='bdi_detail'),
    url(r'^endre/(?P<pk>[0-9]+)$', views.BDIUpdateView.as_view(), name='bdi_update'),
    url(r'^mappe/(?P<pk>[0-9]+)/bdilagret$', views.BDIFinishedView.as_view(), name='bdi_finished')
]
