from django.apps import AppConfig


class AssessmentBdiConfig(AppConfig):
    name = 'assessment_bdi'
    verbose_name = 'Kartlegging BDI'
