from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse

from assessments.models import Folder

class BDIModel(models.Model):
    date = models.DateTimeField(default=timezone.now)
    folder = models.ForeignKey(Folder, related_name='bdis')
    
    question01 = models.PositiveSmallIntegerField(default=0)
    question02 = models.PositiveSmallIntegerField(default=0)
    question03 = models.PositiveSmallIntegerField(default=0)
    question04 = models.PositiveSmallIntegerField(default=0)
    question05 = models.PositiveSmallIntegerField(default=0)
    question06 = models.PositiveSmallIntegerField(default=0)
    question07 = models.PositiveSmallIntegerField(default=0)
    question08 = models.PositiveSmallIntegerField(default=0)
    question09 = models.PositiveSmallIntegerField(default=0)
    question10 = models.PositiveSmallIntegerField(default=0)
    question11 = models.PositiveSmallIntegerField(default=0)
    question12 = models.PositiveSmallIntegerField(default=0)
    question13 = models.PositiveSmallIntegerField(default=0)
    question14 = models.PositiveSmallIntegerField(default=0)
    question15 = models.PositiveSmallIntegerField(default=0)
    question16 = models.PositiveSmallIntegerField(default=0)
    question17 = models.PositiveSmallIntegerField(default=0)
    question18 = models.PositiveSmallIntegerField(default=0)
    question19 = models.PositiveSmallIntegerField(default=0)
    question19a = models.BooleanField(default=False)
    question20 = models.PositiveSmallIntegerField(default=0)
    question21 = models.PositiveSmallIntegerField(default=0)
    score = models.PositiveSmallIntegerField(default=0)

    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'BDI-skjemaer'

    def get_absolute_url(self):
        return reverse('assessments:bdi_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return '{}/{}/BDI-{}'.format(self.folder.user, self.folder.name, self.date)
