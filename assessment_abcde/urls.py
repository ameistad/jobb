from django.conf.urls import include, url
from django.views.generic.base import TemplateView

from . import views


urlpatterns = [
    url(r'^mappe/(?P<pk>[0-9]+)/ny$', views.ABCDECreateView.as_view(), name='abcde_create'),
    url(r'^detalj/(?P<pk>[0-9]+)$', views.ABCDEDetailView.as_view(), name='abcde_detail'),
    url(r'^endre/(?P<pk>[0-9]+)$', views.ABCDEUpdateView.as_view(), name='abcde_update')
]