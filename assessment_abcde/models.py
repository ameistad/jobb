from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse

from assessments.models import Folder


class ABCDEModel(models.Model):
    date = models.DateTimeField(default=timezone.now)
    folder = models.ForeignKey(Folder, related_name='abcdes')

    activating_event = models.TextField(blank=True)
    beliefs = models.TextField(blank=True)
    logical_fallacies = models.TextField(blank=True)
    consequences = models.TextField(blank=True)
    dispute = models.TextField(blank=True)
    effects = models.TextField(blank=True)

    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'ABDCDE-skjemaer'

    def get_absolute_url(self):
        return reverse('assessments:abcde_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return '{}/{}/ABCDE-{}'.format(self.folder.user,self.folder.name, self.date)
