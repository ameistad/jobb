from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils import timezone

from assessments.utils import check_user
from .models import ABCDEModel
from assessments.models import Folder


class ABCDECreateView(LoginRequiredMixin, generic.CreateView):
    model = ABCDEModel
    template_name = 'assessment_abcde/abcde_create.html'
    fields = ['folder', 'activating_event', 'beliefs',
              'logical_fallacies', 'consequences', 'dispute', 'effects']
    
    def get(self, request, *args, **kwargs):
        self.folder = get_object_or_404(Folder, pk=self.kwargs['pk'])
        request = check_user(request, self.folder.user)
        return super(ABCDECreateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ABCDECreateView, self).get_context_data(**kwargs)
        context['from_folder'] = self.folder
        context['other_folders'] = Folder.objects.filter(
            user=self.request.user).exclude(pk=self.folder.pk)
        return context

    def form_valid(self, form):
        form.instance.date = timezone.now()
        return super(ABCDECreateView, self).form_valid(form)

class ABCDEDetailView(LoginRequiredMixin, generic.DetailView):
    model = ABCDEModel
    template_name = 'assessment_abcde/abcde_detail.html'
    context_object_name = 'abcde'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        request = check_user(request, self.object.folder.user)
        return super(ABCDEDetailView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return redirect('assessments:folder_detail', self.object.folder.pk)


class ABCDEUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = ABCDEModel
    template_name = 'assessment_abcde/abcde_update.html'
    context_object_name = 'abcde'
    fields = ['folder', 'activating_event', 'beliefs',
              'logical_fallacies', 'consequences', 'dispute', 'effects']

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Check if user is owner of content
        if self.object.folder.user != request.user:
            raise PermissionDenied
        else:
            return super(ABCDEUpdateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ABCDEUpdateView, self).get_context_data(**kwargs)
        context['folders'] = Folder.objects.filter(user=self.request.user)
        return context
