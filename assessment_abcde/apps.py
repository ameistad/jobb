from django.apps import AppConfig


class AssessmentAbcdeConfig(AppConfig):
    name = 'assessment_abcde'
    verbose_name = 'Skjema ABCDE'
