from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from .models import SFilesModel, SFilesCategoryModel


class SFilesView(LoginRequiredMixin, generic.ListView):
    model = SFilesCategoryModel
    template_name = 'sfiles/index.html'
    context_object_name = 'categories'


class SFilesUploadView(UserPassesTestMixin, generic.CreateView):
    model = SFilesModel
    fields = ['file', 'name', 'category']
    template_name = 'sfiles/upload.html'
    context_object_name = 'sfiles'
    success_url = reverse_lazy('sfiles:index')

    def test_func(self):
        user = self.request.user
        return user.is_staff or user.has_perm('sfiles.change_sfilesmodel')

    def get_context_data(self, **kwargs):
        context = super(SFilesUploadView, self).get_context_data(**kwargs)
        context['categories'] = SFilesCategoryModel.objects.all()
        return context


class SFilesDeleteView(UserPassesTestMixin, generic.DeleteView):
    model = SFilesModel
