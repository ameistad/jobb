from django.contrib import admin

from .models import SFilesCategoryModel, SFilesModel


admin.site.register(SFilesCategoryModel)
admin.site.register(SFilesModel)
