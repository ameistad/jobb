import os

from django import template

register = template.Library()

@register.filter(name='get_filename')
def get_filename(inputfile):
    return os.path.basename(inputfile.file.name)

@register.filter(name='get_fa_icon')
def get_fa_icon(inputfile):
    fa_icons = {
        'pdf': '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>',
        'doc': '<i class="fa fa-file-word-o" aria-hidden="true"></i>',
        'mp4': '<i class="fa fa-file-video-o" aria-hidden="true"></i>',
        'jpg': '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
        'png': '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
        'gif': '<i class="fa fa-file-image-o" aria-hidden="true"></i>'
    }

    try:
        return fa_icons[inputfile[len(inputfile) - 3:]]
    except KeyError:
        return '<i class="fa fa-file-o"></i>'


