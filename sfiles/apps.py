from django.apps import AppConfig


class SfilesConfig(AppConfig):
    name = 'sfiles'
    verbose_name = 'Dokumenter'
