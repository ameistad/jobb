from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.SFilesView.as_view(), name='index'),
    url(r'^lastopp/$', views.SFilesUploadView.as_view(), name='upload'),
]
