from django.db import models
from django.utils import timezone

class SFilesCategoryModel(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'Kategorier'

    def __str__(self):
        return self.name


class SFilesModel(models.Model):
    file = models.FileField(upload_to='files')
    name = models.CharField(max_length=255)
    date = models.DateTimeField(default=timezone.now)
    category = models.ForeignKey(SFilesCategoryModel, related_name='sfiles')

    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'Filer'

    def __str__(self):
        return self.name
