from django.conf.urls import include, url

from . import views


urlpatterns = [
    url(r'^mappe/(?P<pk>[0-9]+)/ny$', views.PSWQCreateView.as_view(), name='pswq_create'),
    url(r'^detalj/(?P<pk>[0-9]+)$', views.PSWQDetailView.as_view(), name='pswq_detail'),
    url(r'^endre/(?P<pk>[0-9]+)$', views.PSWQUpdateView.as_view(), name='pswq_update'),
    url(r'^mappe/(?P<pk>[0-9]+)/pswqlagret$',
        views.PSWQFinishedView.as_view(), name='pswq_finished')
]
