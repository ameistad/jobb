from django.apps import AppConfig


class AssessmentPswqConfig(AppConfig):
    name = 'assessment_pswq'
    verbose_name = 'Kartlegging PSWQ'
