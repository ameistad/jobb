from django import template

register = template.Library()

@register.filter(name='pswq_verbal')
def pswq_verbal(score):
    if score <= 15:
        return 'ingen grad av bekymring'
    elif score <= 39:
        return 'lav grad av bekymring'
    elif score <= 59:
        return 'moderat grad av bekymring'
    else:
        return 'Høy grad av bekymring'