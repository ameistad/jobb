# Return the reverse score for pswq
def pswq_reverse_score(value):
    reverse_score = {1: 5, 2: 4, 3: 3, 4: 2, 5: 1}
    return reverse_score[value]
