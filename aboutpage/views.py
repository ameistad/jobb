import sys
from django import VERSION as django_version

from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from django.contrib.auth.models import User
from assessments.models import Folder

class AboutPageView(LoginRequiredMixin, TemplateView):
    template_name = 'aboutpage/aboutpage.html'

    def get_context_data(self, **kwargs):
        context = super(AboutPageView, self).get_context_data(**kwargs)
        context['platform'] = sys.platform
        context['python_version'] = sys.version[:6]
        context['django_version'] = '{}.{}.{}'.format(django_version[0],
                                                      django_version[1],
                                                      django_version[2])
        context['number_of_users'] = User.objects.all().count()
        context['number_of_folders'] = Folder.objects.all().count()
        return context
