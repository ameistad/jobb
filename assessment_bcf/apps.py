from django.apps import AppConfig


class AssessmentBcfConfig(AppConfig):
    name = 'assessment_bcf'
    verbose_name ='Beck kasusformulering'
