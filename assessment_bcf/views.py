from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from django.forms import modelform_factory, formset_factory
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import transaction, IntegrityError

from .models import BCFModel, BCFExtraModel
from .forms import BCFExtraForm
from assessments.models import Folder

@login_required
def bcf_create_view(request, pk):
    folder = get_object_or_404(Folder, pk=pk)

    if folder.user != request.user:
        raise PermissionDenied

    # Generate forms
    BCFForm = modelform_factory(BCFModel, exclude=('date',))
    BCFFormSet = formset_factory(BCFExtraForm, extra=1)

    if request.method == 'POST':
        bcf_form = BCFForm(request.POST)

        if bcf_form.is_valid():
            new_bcf_form = bcf_form.save(commit=False)
            new_bcf_form.date = timezone.now()
            new_bcf_form.save()

            bcf_extra_form = BCFFormSet(request.POST, prefix='bcf_extra')

            if bcf_extra_form.is_valid():
                bcf_extra_data = []
                
                for field in bcf_extra_form:
                    situation = field.cleaned_data.get('situation')
                    automatic_thought = field.cleaned_data.get('automatic_thought')
                    meaning_automatic_thought = field.cleaned_data.get('meaning_automatic_thought')
                    emotion = field.cleaned_data.get('emotion')
                    behavior = field.cleaned_data.get('behavior')

                    bcf_extra_data.append(BCFExtraModel(
                        parent_model=new_bcf_form,
                        situation=situation,
                        automatic_thought=automatic_thought,
                        meaning_automatic_thought=meaning_automatic_thought,
                        emotion=emotion,
                        behavior=behavior))

                # Bulk save form date to extra model
                try:
                    with transaction.atomic():
                        BCFExtraModel.objects.bulk_create(bcf_extra_data)
                
                except IntegrityError:
                    print('IntegrityError')

            else:
                print('Extra not valid')

        return redirect('assessments:bcf_detail', new_bcf_form.pk)

    else:
        bcf_form = BCFForm()
        bcf_extra_form = BCFFormSet(prefix='bcf_extra')

        other_folders = Folder.objects.filter(user=request.user).exclude(pk=pk)

        context = {
            'bcf_form': bcf_form,
            'bcf_extra_form': bcf_extra_form,
            'from_folder': folder,
            'other_folders': other_folders
        }

        return render(request, 'assessment_bcf/bcf_create.html', context)


class BCFDetailView(LoginRequiredMixin, generic.DetailView):
    model = BCFModel
    template_name = 'assessment_bcf/bcf_detail.html'
    context_object_name = 'bcf'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        # Check if user is owner of content
        if self.object.folder.user != request.user:
            raise PermissionDenied
        else:
            return super(BCFDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BCFDetailView, self).get_context_data(**kwargs)
        context['bcf_extra'] = BCFExtraModel.objects.filter(
            parent_model=self.object)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()

        return redirect('assessments:folder_detail', self.object.folder.pk)
