from django.contrib import admin

from .models import BCFModel, BCFExtraModel

admin.site.register(BCFModel)
admin.site.register(BCFExtraModel)
