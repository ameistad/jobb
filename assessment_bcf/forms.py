from django import forms

class BCFExtraForm(forms.Form):
    situation = forms.CharField(required=False)
    automatic_thought = forms.CharField(required=False)
    meaning_automatic_thought = forms.CharField(required=False)
    emotion = forms.CharField(required=False)
    behavior = forms.CharField(required=False)
