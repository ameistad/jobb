from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse

from assessments.models import Folder


class BCFModel(models.Model):
    date = models.DateTimeField(default=timezone.now)
    folder = models.ForeignKey(Folder, related_name='bcfs')

    relevant_childhood_data = models.TextField(blank=True)
    core_beliefs = models.TextField(blank=True)
    conditional_assumptions = models.TextField(blank=True)
    coping_strategies = models.TextField(blank=True)

    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'Beck kasusformuleringer'

    def get_absolute_url(self):
        return reverse('assessments:bcf_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return '{}/{}/BCF-{}'.format(self.folder.user, self.folder.name, self.date)

class BCFExtraModel(models.Model):
    parent_model = models.ForeignKey(BCFModel)

    situation = models.TextField(blank=True)
    automatic_thought = models.TextField(blank=True)
    meaning_automatic_thought = models.TextField(blank=True)
    emotion = models.TextField(blank=True)
    behavior = models.TextField(blank=True)

    class Meta:
        verbose_name_plural = 'ABC-skjema'

    def __str__(self):
        return '{}/{}'.format(self.parent_model, self.situation)
