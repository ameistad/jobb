from django.conf.urls import include, url
from django.views.generic.base import TemplateView

from . import views


urlpatterns = [
    url(r'^mappe/(?P<pk>[0-9]+)/ny$', views.bcf_create_view, name='bcf_create'),
    url(r'^detalj/(?P<pk>[0-9]+)$', views.BCFDetailView.as_view(), name='bcf_detail'),
    # url(r'^endre/(?P<pk>[0-9]+)$', views.bcf_update_view, name='bcf_update'),
    # url(r'^mappe/(?P<pk>[0-9]+)/oppsummering$', views.BCFSummaryView.as_view(), 
    #     name='bcf_summary'),
]
