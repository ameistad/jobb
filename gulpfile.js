// Include Gulp
var gulp = require('gulp');

// Include Our Plugins
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var autoprefixer = require('autoprefixer'); 
var postcss = require('gulp-postcss');

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('static/scss/style.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('static/css'));
});

// Concatenate
// gulp.task('scripts', function() {
//     return gulp.src('static/js/*.js')
//     .pipe(concat('project.js'))
//     .pipe(gulp.dest('static/js'));
// });

// PostCSS processor
gulp.task('css', function () { 
    var processors = [
        autoprefixer({browsers: ['last 1 version']}), 
    ];
    return gulp.src('static/css/*.css') 
        .pipe(postcss(processors)) 
        .pipe(gulp.dest('static/css'))
});

// Watch Files For Changes
gulp.task('watch', function() { 
    // gulp.watch('static/js/*.js', ['scripts']); 
    gulp.watch('static/scss/*.scss', ['sass']); 
    gulp.watch('static/css/*.css', ['css']);
});
// Default Task
gulp.task('default', ['sass', 'css', 'watch']);
// gulp.task('default', ['sass', 'css', 'scripts', 'watch']);