from django import forms
from registration.forms import RegistrationForm

NON_TK_EMAIL = ('Det er kun tillatt å registrere seg med e-postadresse'
                ' fra trondheim kommune (brukernavn@trondheim.kommune.no).')


class RegistrationFormOnlyTK(RegistrationForm):
    '''
    Only allow email from *@trondheim.kommune.no

    '''

    def clean_email(self):
        email_domain = self.cleaned_data['email'].split('@')[1]
        if email_domain != 'trondheim.kommune.no':
            raise forms.ValidationError(NON_TK_EMAIL)
        return self.cleaned_data['email']