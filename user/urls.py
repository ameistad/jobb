# -*- coding: utf-8 -*-
"""
URLconf for registration and activation, using django-registration's
HMAC activation workflow.
"""

from django.conf.urls import include, url
from django.views.generic.base import TemplateView

from . import views


urlpatterns = [
    url(r'^bruker/(?P<pk>[0-9]+)$', views.Dashboard.as_view(), name="dashboard"),
    url(r'^aktiver/ferdig/$',
        TemplateView.as_view(
            template_name='user/activation_complete.html'
        ),
        name='registration_activation_complete'),
    # The activation key can make use of any character from the
    # URL-safe base64 alphabet, plus the colon as a separator.
    url(r'^aktiver/(?P<activation_key>[-:\w]+)/$',
        views.CustomActivationView.as_view(),
        name='registration_activate'),
    url(r'^ny/$',
        views.CustomRegistrationView.as_view(template_name='user/registration.html'),
        name='register'),
    url(r'^ny/ferdig/$',
        TemplateView.as_view(
            template_name='user/registration_complete.html'
        ),
        name='registration_complete'),
    url(r'^ny/stengt/$',
        TemplateView.as_view(
            template_name='user/registration_closed.html'
        ),
        name='registration_disallowed'),
    url(r'', include('user.auth_urls')),
]