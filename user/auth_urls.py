"""
URL patterns for the views included in ``django.contrib.auth``.
Including these URLs (via the ``include()`` directive) will set up the
following patterns based at whatever URL prefix they are included
under:
* User login at ``login/``.
* User logout at ``logout/``.
* The two-step password change at ``password/change/`` and
  ``password/change/done/``.
* The four-step password reset at ``password/reset/``,
  ``password/reset/confirm/``, ``password/reset/complete/`` and
  ``password/reset/done/``.
The URLconfs in the built-in registration workflows already have an
``include()`` for these URLs, so if you're using one of them it is not
necessary to manually include these views.
"""

from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views as custom_views


urlpatterns = [
    url(r'^logginn/$',
        auth_views.login,
        {'template_name': 'user/login.html'},
        name='login'),
    url(r'^loggut/$',
        custom_views.custom_logout_view,
        name='logout'),
    # url(r'^loggut/$',
    #     auth_views.logout,
    #     {'template_name': 'user/logout.html'},
    #     name='logout'),

    # Password change
    url(r'^passord/endre/$',
        auth_views.password_change,
        {'template_name': 'user/password/password_change.html',
         'post_change_redirect': 'user:auth_password_change_done'},
        name='auth_password_change'),
    url(r'^passord/endre/ferdig/$',
        auth_views.password_change_done,
        {'template_name': 'user/password/password_change_done.html'},
        name='auth_password_change_done'),

    # Password reset
    url(r'^passord/reset/$',
        auth_views.password_reset,
        {'template_name': 'user/password/password_reset.html',
         'post_reset_redirect': 'user:auth_password_reset_done',
         'email_template_name': 'user/password/password_reset_email.txt'},
        name='auth_password_reset'),
    url(r'^passord/reset/godta/(?P<uidb64>[0-9A-Za-z_\-]+)/'
        r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,
        {'template_name': 'user/password/password_reset_confirm.html',
         'post_reset_redirect': 'user:auth_password_reset_complete'},
        name='auth_password_reset_confirm'),
    url(r'^passord/reset/ferdig/$',
        auth_views.password_reset_complete,
        {'template_name': 'user/password/password_reset_complete.html'},
        name='auth_password_reset_complete'),
    url(r'^passord/reset/ok/$',
        auth_views.password_reset_done,
        {'template_name': 'user/password/password_reset_done.html'},
        name='auth_password_reset_done'),
]