from django.views import generic
from django.contrib.auth import get_user_model
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect

from registration.backends.hmac.views import RegistrationView, ActivationView
from .forms import RegistrationFormOnlyTK
from assessments.utils import check_user


@login_required
def custom_logout_view(request):
    logout(request)
    return redirect('index')


class Dashboard(LoginRequiredMixin, generic.DetailView):
    template_name = 'user/dashboard.html'
    model = get_user_model()
    context_object_name = 'user'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        request = check_user(request, self.object)
        return super(Dashboard, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        logout(request)
        self.object.delete()
        return redirect('index')

class CustomRegistrationView(RegistrationView):

    email_body_template = 'user/activation_email.txt'
    email_subject_template = 'user/activation_email_subject.txt'
    template_name = 'user/registration.html'
    #Remove this to allow all domains
    form_class = RegistrationFormOnlyTK

    def get_success_url(self, user):
        return ('user:registration_complete', (), {})


class CustomActivationView(ActivationView):
    template_name = 'user/activation_complete.html'

    def get_success_url(self, user):
        return ('user:registration_activation_complete', (), {})
